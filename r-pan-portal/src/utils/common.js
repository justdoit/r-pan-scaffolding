/**
 * Created by rubin on 2020/6/6.
 */

'use strict';

import { getToken } from './cookie'

let panUtil = {
    checkUsername(username) {
        return !!username && /^[0-9A-Za-z]{6,16}$/.test(username)
    },
    checkPassword(password) {
        return !!password && password.length >= 8 && password.length <= 16
    },
    showOperation(dom) {
        let parentDiv = dom.firstElementChild
        if (parentDiv && parentDiv.classList.contains('el-tooltip')) {
            let div = parentDiv.lastElementChild
            div.style.display='inline-block'
        }
    },
    hiddenOperation(dom) {
        let parentDiv = dom.firstElementChild
        if (parentDiv && parentDiv.classList.contains('el-tooltip')) {
            let div = parentDiv.lastElementChild
            div.style.display='none'
        }
    },
    getFileFontElement(type) {
        let tagStr = 'fa fa-file'
        switch (type) {
            case 0:
                tagStr = 'fa fa-folder-o'
                break
            case 2:
                tagStr = 'fa fa-file-archive-o'
                break
            case 3:
                tagStr = 'fa fa-file-excel-o'
                break
            case 4:
                tagStr = 'fa fa-file-word-o'
                break
            case 5:
                tagStr = 'fa fa-file-pdf-o'
                break
            case 6:
                tagStr = 'fa fa-file-text-o'
                break
            case 7:
                tagStr = 'fa fa-file-image-o'
                break
            case 8:
                tagStr = 'fa fa-file-audio-o'
                break
            case 9:
                tagStr = 'fa fa-file-video-o'
                break
            case 10:
                tagStr = 'fa fa-file-powerpoint-o'
                break
            case 11:
                tagStr = 'fa fa-file-code-o'
                break
            default:
                break
        }
        return tagStr
    },
    getPreviewUrl(fileId) {
        return 'http://127.0.0.1:7000/preview?fileId=' + fileId + '&authorization=' + getToken()
    },
    getUrlPrefix() {
        return 'http://127.0.0.1:7000'
    },
    getChunkSize() {
        return 1024 * 1024 * 2
    },
    getMaxFileSize() {
        return 1024 * 1024 * 1024 * 3
    }
}

export default panUtil
